import React, {Component} from 'react';
import './App.css';
import List from './List';

class App extends Component {

    state = {
        users: []
    };

    componentDidMount() {
        this.requestData();
    }

    requestData = () => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET',
            'https://jsonplaceholder.typicode.com/users/', true);
        xhr.onload = () => {
            if (xhr.status === 200) {
                const users = JSON.parse(xhr.response);
                this.setState({users})
            }
        };
        xhr.send(null);
    };

    render() {
        const users = this.state.users.map(user => (
            <ul key={user.id}>
                name: <strong>{user.name}</strong>
                <br/>
                city: <strong>{user.address.city}</strong>
            </ul>
        ));
        return (
            <div className="App">
                <List users={users}/>
            </div>
        );
    }
}

export default App;
