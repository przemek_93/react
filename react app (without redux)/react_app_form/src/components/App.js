import React, {Component} from 'react';
import './App.css';

class App extends Component {

    state = {
        username: '',
        email: '',
        password: '',
        checkbox: false,
        message: '',
        errors: {
            username: false,
            email: false,
            password: false,
            checkbox: false,
        }
    };

    messages = {
        username_incorrect: "Nazwa musi być dłuższa niż 10 znaków i nie może zawierać spacji",
        email_incorrect: "Brak @ w emailu",
        password_incorrect: "Hasło musi mieć 8 znaków",
        checkbox_incorrect: "Zatwierdź zgodę",
    };

    handleChange = (e) => {
        const name = e.target.name;
        const type = e.target.type;

        if (type === "text" || type === "password" || type === "email") {
            const value = e.target.value;
            this.setState({
                [name]: value
            })
        } else if (type === "checkbox") {
            const checked = e.target.checked;
            this.setState({
                [name]: checked
            })
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();

        const validation = this.formValidation();

        if (validation.correct) {
            this.setState({
                username: '',
                email: '',
                password: '',
                checkbox: false,
                message: 'Formularz został wysłany',
                errors: {
                    username: false,
                    email: false,
                    password: false,
                    checkbox: false,
                }
            })
        } else {
            this.setState({
                errors: {
                    username: !validation.username,
                    email: !validation.email,
                    password: !validation.password,
                    checkbox: !validation.checkbox,
                }
            })
        }
    };

    formValidation = () => {
        let username = false;
        let email = false;
        let password = false;
        let checkbox = false;
        let correct = false;

        if (this.state.username.length > 10 && this.state.username.indexOf(' ') === -1) {
            username = true;
        }

        if (this.state.email.indexOf('@') !== -1) {
            email = true;
        }

        if (this.state.password.length >= 8) {
            password = true;
        }

        if (this.state.checkbox) {
            checkbox = true;
        }

        if (username && email && password && checkbox) {
            correct = true;
        }

        return ({
            correct,
            username,
            email,
            password,
            checkbox
        })
    };

    componentDidUpdate() {
        if (this.state.message !== '') {
            setTimeout(() => this.setState({
                message: ''
            }),3000)
        }
    }


    render() {
        const {username, email, password, checkbox} = this.state;
        return (
            <div className="App">
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="user">Twoje imię:
                        <input type="text" id="user" name="username" value={username} onChange={this.handleChange}/>
                        {this.state.errors.username && <span>{this.messages.username_incorrect}</span>}
                    </label>
                    <br/>
                    <br/>
                    <label htmlFor="email">Twój email:
                        <input type="email" id="email" name="email" value={email} onChange={this.handleChange}/>
                        {this.state.errors.email && <span>{this.messages.email_incorrect}</span>}
                    </label>
                    <br/>
                    <br/>
                    <label htmlFor="password">Twoje hasło:
                        <input type="password" id="password" name="password" value={password}
                               onChange={this.handleChange}/>
                        {this.state.errors.password && <span>{this.messages.password_incorrect}</span>}
                    </label>
                    <br/>
                    <br/>
                    <label htmlFor="checkbox">
                        <input type="checkbox" id="checkbox" name="checkbox" value={checkbox}
                               onChange={this.handleChange}/>
                        {this.state.errors.checkbox && <span>{this.messages.checkbox_incorrect}</span>}
                        Wyrażam zgodę
                    </label>
                    <br/>
                    <br/>
                    <button>Zapisz się</button>
                </form>
                {this.state.message && <h3>{this.state.message}</h3>}
            </div>
        );
    }
}

export default App;
