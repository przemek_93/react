import React from 'react'

const ErrorPage = () => (
    <div className="error">
        <h1>Page not found!</h1>
    </div>
);

export default ErrorPage;