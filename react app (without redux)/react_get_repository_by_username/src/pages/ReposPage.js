import React from 'react'

const ReposPage = (props) => {
    const list = props.data.map(data => (
        <>
            <ul className="list">
                <a href={data.html_url} target="_blank" rel="noopener noreferrer" key={data.name}>{data.name}</a>
            </ul>
        </>
    ));

    return (
        <div className="repos">
            {list.length ? (<h5>Repositories:</h5>) : (<h5>Please enter the owner name</h5>)}
            {list}
        </div>
    )
};

export default ReposPage;