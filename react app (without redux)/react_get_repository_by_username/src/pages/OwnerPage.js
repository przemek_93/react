import React from 'react'
import "../styles/OwnerPage.css"

const OwnerPage = (props) => {

    const name = props.data.map(data => (data.owner.login));
    const avatar = props.data.map(data => (data.owner.avatar_url));
    const technology = props.data.map(data => (
        <ul>
            {data.language}
        </ul>
    ));
    return (
        <>
            <div className="owner">
                {name.length ? (<h5>Login:</h5>) : (<h5>Please enter the owner name</h5>)}
                <img className="owner" src={avatar[0]} alt={name[0]}/>
                <span className="data">
                    {name[0]}
                </span>
                {name.length ? (<h5>Technology:</h5>) : null}
                <span className="data">
                    {technology}
                </span>
            </div>
        </>
    )
};

export default OwnerPage;