import React from 'react';
import {Route, Switch} from 'react-router-dom';
import '../styles/Page.css';

import HomePage from "../pages/HomePage";
import ReposPage from "../pages/ReposPage";
import OwnerPage from "../pages/OwnerPage";
import ErrorPage from "../pages/ErrorPage";

const Page = (props) => {

    return (
        <>
            <Switch>
                <Route path="/" exact component={() => <HomePage data={props.data}/>} />
                <Route path="/repos" exact component={() => <ReposPage data={props.data}/>} />
                <Route path="/owner" exact component={() => <OwnerPage data={props.data}/>} />
                <Route exact component={ErrorPage}/>
            </Switch>
        </>
    )
};

export default Page