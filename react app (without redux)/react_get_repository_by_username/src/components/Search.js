import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import "../styles/Search.css"
import banner from "../images/banner.png"

const Search = (props) => {

    return (
        <>
            <div className="input">
                <div className="input-group mb-3">
                    <input type="text" className="form-control" placeholder="Owner's name"
                           aria-label="Owner's name" aria-describedby="basic-addon2" onChange={props.input}/>
                    <div className="input-group-append">
                        <button onClick={props.click} className="btn btn-outline-secondary" type="button">Search
                        </button>
                    </div>
                </div>
            </div>
            <a href="/"><img src={banner} alt="banner" /></a>
        </>
    )
};

export default Search

