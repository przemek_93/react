import React, {Component} from 'react';
import {BrowserRouter as Router} from 'react-router-dom';

import '../styles/App.css';
import Header from "./Header";
import Footer from "./Footer";
import Page from "./Page";
import Search from "./Search";
import Navigation from "./Navigation";

class App extends Component {

    state = {
        username: '',
        url: '',
        repos: []
    };

    handleDataFetch = () => {
        this.setState({
            repos: []
        });

        fetch(this.state.url)
            .then(response => {
                if (response.ok) {
                    return response;
                }
                throw Error(response.status)
            })
            .then(response => response.json())
            .then(data => {
                this.setState(prevState => ({
                    repos: prevState.repos.concat(data)
                }))
            })
            .catch(error => console.log(error + " Error!!! "));

    };

    handleUserNameInput = (e) => {
        this.setState({
            url: `https://api.github.com/users/${e.target.value}/repos`,
        })
    };

    render() {
        console.log(this.state.url);
        return (
            <Router>
                <div className="app">
                    <header>
                        {<Header/>}
                    </header>

                    <section className="search">
                        {<Search click={this.handleDataFetch} input={this.handleUserNameInput}/>}
                    </section>

                    <main>
                        <div className="navigation">
                            {<Navigation data={this.state.repos}/>}
                        </div>

                        <div className="page">
                            {<Page data={this.state.repos}/>}
                        </div>
                    </main>

                    <footer>
                        {<Footer/>}
                    </footer>
                </div>
            </Router>
        );
    }
}

export default App;
