import React from 'react';
import {NavLink} from "react-router-dom";
import '../styles/Navigation.css'

const list = [
    {name: "Repositories", path: "/repos", exact: true},
    {name: "Owner", path: "/owner"}
];

const Navigation = (props) => {

    const numberOfRepos = props.data.map(data => data.name).length;
    console.log(numberOfRepos);

    const menu = list.map(item => (
        <li key={item.name}>
            <NavLink to={item.path} exact={item.exact ? item.exact : false}>{item.name}</NavLink>
            {item.name === "Repositories" ? {numberOfRepos} : null }
        </li>
    ));

    return (
        <nav className="main">
            <ul>
                {menu}
            </ul>
        </nav>
    )
};

export default Navigation