import React from 'react';

const UsersList = (props) => {
    console.log(props.users);
    const users = props.users.map(user => (
        <div key={user.name.salt}>
            <img src={user.picture.large} alt={user.name.last}/>
            <p>First Name: <strong>{user.name.first}</strong>
                <br/>
                Last Name: <strong>{user.name.last}</strong></p>
        </div>
    ));

    return (
        <ul>
            {users}
            <br/>
        </ul>
    )
};

export default UsersList