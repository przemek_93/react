import React, {Component} from 'react';
import {BrowserRouter as Router} from 'react-router-dom';

import '../styles/App.css';
import Header from "./Header";
import Footer from "./Footer";
import Page from "./Page";
import Navigation from "./Navigation";

class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <header>
                        {<Header/>}
                    </header>

                    <main>
                        <aside>
                            {<Navigation/>}
                        </aside>

                        <section className="page">
                            {<Page/>}
                        </section>
                    </main>

                    <footer>
                        {<Footer/>}
                    </footer>
                </div>
            </Router>
        );
    }
}

export default App;
