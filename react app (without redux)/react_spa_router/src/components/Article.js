import React from 'react';

const Article = (props) => {
    return (
        <article className="article">
            <h4>{props.article.title}</h4>
            <h6>{props.article.author}</h6>
            <p>{props.article.text}</p>
        <br />
        </article>
    );
};

export default Article;
