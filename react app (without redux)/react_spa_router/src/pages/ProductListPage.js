import React from 'react';
import {Link} from 'react-router-dom';
import '../styles/ListPage.css'

const products = ["Car", "Motorcycle", "Bike"];

const ProductListPage = () => {

    const list = products.map(product => (
        <li key={product}>
            <Link to={`/product/${product}`}>{product}</Link>
        </li>
    ));

    return (
        <div className="products">
            <h4>Lista Produktów:</h4>
            <br/>
            {list}
        </div>
    );
};

export default ProductListPage;
