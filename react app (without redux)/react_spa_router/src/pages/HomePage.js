import React from 'react'

import Article from '../components/Article'

const articles = [
    {
        id: 1,
        title: "Artykuł 1",
        author: "J.Nowak",
        text: "Onuss sunt abnobas de pius fuga.Extend is not simple in heavens, the pyramid, or chaos, but everywhere.All those courages will be lost in tragedies like attitudes in alignments"
    },
    {
        id: 2,
        title: "Artykuł 2",
        author: "J.Kowalski",
        text: "Onuss sunt abnobas de pius fuga.Extend is not simple in heavens, the pyramid, or chaos, but everywhere.All those courages will be lost in tragedies like attitudes in alignments"
    },
    {
        id: 3,
        title: "Artykuł 3",
        author: "K.Nowak",
        text: "Onuss sunt abnobas de pius fuga.Extend is not simple in heavens, the pyramid, or chaos, but everywhere.All those courages will be lost in tragedies like attitudes in alignments"
    }
];

const HomePage = () => {

    const artList = articles.map(article => (
        <Article key={article.id} article={article}/>
    ));

    return (
        <div className="home">
            {artList}
        </div>
    );
};

export default HomePage;