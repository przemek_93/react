import React from 'react';
import {Route, Redirect} from 'react-router-dom';

const permission = true;

const AdminPage = () => {
    return (
        <Route render={() => (
            permission ? (<Redirect to="/login"/>) : (<h3>Panel admina - dzień dobry</h3>)
        )}/>
    );
};

export default AdminPage;