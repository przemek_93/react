import React from 'react';
import "./Word.css";

const Word = (props) => (
    <li>
        <div>Słowo po angielsku: <strong>{props.english}</strong></div>
        <div>Tłumaczenie na polski:<strong>{props.polish}</strong></div>
    <br/>
    </li>
);

export default Word