import React from 'react';
import '../styles/Score.css';

function Score(props) {
    return (
        <div className="score">
            {props.result}
        </div>
    );
}

export default Score;
