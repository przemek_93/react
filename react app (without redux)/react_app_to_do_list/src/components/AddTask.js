import React, {Component} from 'react';
import './App.css';

class AddTask extends Component {

    minDate = new Date().toISOString().slice(0, 10);

    state = {
        text: '',
        important: false,
        date: this.minDate
    };

    handleText = (e) => {
        this.setState({
            text: e.target.value
        })
    };

    handleCheckbox = (e) => {
        this.setState({
            important: e.target.checked
        })
    };

    handleDate = (e) => {
        this.setState({
            date: e.target.value
        })
    };

    handleClick = () => {
        const {text, date, important} = this.state;
        const add = this.props.add(text, date, important);
        if (add) {
            this.setState({
                text: '',
                checked: false,
                date: this.minDate
            })
        }
    };

    render() {
        const {text, important, date} = this.state;
        return (
            <div className="form">
                <form>
                    <input type="text" placeholder="dodaj zadanie" value={text} onChange={this.handleText}/>
                    <input type="checkbox" id="important" checked={important} onChange={this.handleCheckbox}/>
                    <label htmlFor="important">Priorytet</label>
                    <br/>
                    <label htmlFor="date">Do kiedy zrobić</label>
                    <input type="date" value={date} onChange={this.handleDate}/>
                    <br/>
                    <button onClick={this.handleClick}>DODAJ</button>
                </form>
                <hr/>
            </div>
        );
    }
}

export default AddTask;