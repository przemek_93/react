import React, {Component} from 'react';
import './App.css';
import AddTask from "./AddTask";
import TaskList from "./TaskList";

class App extends Component {

  counter = 4;

    state = {
        tasks: [
            {
                id: 0,
                text: 'Przykładowy task',
                date: '2019-09-23',
                important: false,
                active: true,
                finishDate: null
            },
            {
                id: 1,
                text: 'Przykładowy task 1',
                date: '2019-09-23',
                important: false,
                active: true,
                finishDate: null
            },
            {
                id: 2,
                text: 'Przykładowy task 2',
                date: '2019-09-23',
                important: true,
                active: true,
                finishDate: null
            },
            {
                id: 3,
                text: 'Przykładowy task 3',
                date: '2019-09-23',
                important: true,
                active: true,
                finishDate: null
            },
        ]
    };

    deleteTask = (id) => {
        let tasks = [...this.state.tasks];
        tasks = tasks.filter(task => task.id !== id);
        this.setState({
            tasks
        })
    };

    doneTask = (id) => {
        const tasks = Array.from(this.state.tasks);
        tasks.forEach(task => {
            if (task.id === id) {
                task.active = false;
                task.finishDate = new Date().getTime()
            }
        });
        this.setState({
            tasks
        })
    };

    addTask = (text, date, important) => {
        const task = {
            id: this.counter,
            text,
            date,
            important,
            active: true,
            finishDate: null
        };
        this.counter++;

        this.setState(prevState => ({
            tasks: [...prevState.tasks, task]
        }));

        return true;
    };

    render() {
        return (
            <div className="App">
                <AddTask add={this.addTask}/>
                <TaskList tasks={this.state.tasks} delete={this.deleteTask} done={this.doneTask}/>
            </div>
        );
    }
}

export default App;
