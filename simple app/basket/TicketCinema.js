const ValidationMessage = (props) => {
    const {txt} = props;
    return (
        <p>{props.txt}</p>
    )
};

const OrderForm = (props) => {
    const {submit, change, check} = props;
    return (
        <form onSubmit={submit}>
            <input type="checkbox" id="age" onChange={change} checked={check}/>
            <label htmlFor="age">Mam co najmniej 16 lat </label>
            <br/>
            <button>Kup bilet</button>
        </form>
    );
};

class TicketShop extends React.Component {

    state = {
        isConfirmed: false,
        isFormSubmitted: false,
    };

    handleCheckboxChange = () => {
        this.setState({
            isConfirmed: !this.state.isConfirmed,
            isFormSubmitted: false
        })
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        if (!this.state.isFormSubmitted) {
            this.setState({
                isFormSubmitted: true
            })
        }
    };

    displayMessage = () => {
        if (this.state.isFormSubmitted) {
            if (this.state.isConfirmed) {
                return <ValidationMessage txt="Możesz obejrzeć film!"/>
            } else {
                return <ValidationMessage txt="Nie możesz obejrzeć filmu!"/>

            }
        } else {
            return null
        }
    };

    render() {
        const {isConfirmed, isFormSubmitted} = this.state;

        return (
            <React.Fragment>
                <h1>Kup bilet na horror roku!</h1>
                <OrderForm
                    change={this.handleCheckboxChange}
                    submit={this.handleFormSubmit}
                    check={isConfirmed}
                />
                {this.displayMessage()}
            </React.Fragment>
        )
    }
}

ReactDOM.render(<TicketShop/>, document.getElementById('root'));
