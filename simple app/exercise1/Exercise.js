class Exercise extends React.Component {

    state = {
        number: null,
        content: "",
        contents: ["wróżba 1", "wróżba 2", "wróżba 3", "wróżba 4", "wróżba 5", "wróżba 6"]
    };

    contentList = () => {
        const {number, contents} = this.state;
        if (number === null) {
            return null;
        } else {
            return contents[number];
        }
    };

    handleContentFilter = () => {
        const index = Math.floor(Math.random() * this.state.contents.length);
        this.setState({
            number: index
        })
    };

    handleUpdatedContents = () => {
        if (this.state.content !== "") {
            const contents = this.state.contents.concat(this.state.content);
            this.setState({
                contents,
                content: '',
            });
            alert(`Wróżba dodana, akutalne wróżby to: ${this.state.contents}`)
        }
    };

    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    };

    render() {
        return (
            <React.Fragment>
                <label>
                    <input name="content" type="text" onChange={this.handleInput} value={this.state.content}/>
                    <button onClick={this.handleUpdatedContents}>Dodaj wróżbę</button>
                </label>
                <br/>
                <div>
                    <button onClick={this.handleContentFilter}>Wylosuj wróżbę</button>
                </div>
                <h1>
                    {this.contentList()}
                </h1>
            </React.Fragment>
        )
    }
}

ReactDOM.render(<Exercise/>, document.getElementById('root'));
