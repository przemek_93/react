const List = (props) => {
    const users = props.users.map(user =>
        <ul key={user.name} name={user.name}>
            {user.name}
            <button onClick={props.deleteUser} value={user.name}>Usuń</button>
        </ul>);

    return (
        <header>
            <h2>Użytkownicy</h2>
            {users}
        </header>
    )
};

class Users extends React.Component {

    state = {
        users: [
            {id: 1, name: "Janusz K."},
            {id: 2, name: "Adam M."},
            {id: 3, name: "Pablo W."},
            {id: 4, name: "Jaco A."},
            {id: 5, name: "John R."}
        ]
    };

    handleDeleteUser = (e) => {
        const users = this.state.users;
        const array = [...users];
        const index = array.findIndex(user => user.name === e.target.value);
        users.splice(index, 1);
        this.setState({
            users: users
        })
    };

    render() {
        const {users} = this.state;
        return (
            <React.Fragment>
                <List users={users} deleteUser={this.handleDeleteUser}/>
            </React.Fragment>
        )
    }
}

ReactDOM.render(<Users/>, document.getElementById('root'));
