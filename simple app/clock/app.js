class Button extends React.Component {

    state = {
        active: true
    };

    handleActive = () => {
        this.setState( prevState => ({
            active: !this.state.active
        }))
    };

    render () {
        const {active} = this.state;
        return (
            <div>
                <button onClick={this.handleActive}>{active === true ? 'Wyłącz' : 'Włącz' }</button>
                {active && <App />}
            </div>
        )
    }
}

class App extends React.Component {

    state = {
        time: this.getTime()
    };

    getTime() {
        const currentTime = new Date();
        return ({
            hours: currentTime.getHours(),
            minutes: currentTime.getMinutes(),
            seconds: currentTime.getSeconds()
        })
    }

    setTime() {
        const time = this.getTime();
        this.setState({
            time: time
        })
    }

    componentDidMount() {
        this.interval = setInterval(this.setTime.bind(this), 1000)
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    render() {
        const {hours, minutes, seconds} = this.state.time;
        return (
            <div>
                {hours} : {minutes} : {seconds}
            </div>
        )
    }
}

ReactDOM.render(<Button/>, document.getElementById('root'));
