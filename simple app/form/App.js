class Form extends React.Component {

    state = {
        city: "",
        text: "",
        isLiked: true,
        number: 2
    };

    handleChange = (e) => {
        if (e.target.type === "checkbox") {
            this.setState({
                [e.target.name]: e.target.checked,
            })
        } else {
            this.setState({
                [e.target.name]: e.target.value,
            })
        }
    };

    render() {
        const {city, text, isLiked, number} = this.state;
        const handleChange = this.handleChange;

        return (
            <div>
                <label>
                    Podaj miasto
                    <input name="city" value={city} onChange={handleChange} type="text"/>
                </label>
                <br/>
                <label>
                    Napisz coś o mieście
                    <textarea name="text" value={text} onChange={handleChange} type="text"></textarea>
                </label>
                <br/>
                <label>
                    Lubisz to miasto ?
                    <input name="isLiked" checked={isLiked} onChange={handleChange} type="checkbox"></input>
                </label>
                <br/>
                <label>
                    Ile razy byliście ?
                    <select name="number" value={number} onChange={handleChange}>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="more">więcej</option>
                    </select>
                </label>
            </div>
        )
    }
}


