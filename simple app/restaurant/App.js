class App extends React.Component {

    state = {
        items: [
            { id: 1, name: "herbata", active: true},
            { id: 2, name: "kawa", active: false},
            { id: 3, name: "woda", active: true},
            { id: 4, name: "ryż", active: false},
            { id: 5, name: "kasza", active: false},
            { id: 6, name: "ziemniaki", active: false}
        ]
    };

    handleChangeStatus = (id) => {
        const items = this.state.items.map(item => {
            if(id === item.id) {
                item.active = !item.active
            }
            return item
        });

        this.setState({
            items: items
        })
    };

    render() {
        const {items} = this.state;

        return (
            <React.Fragment>
                <Header items={items}/>
                <ListItems items={items} changeStatus={this.handleChangeStatus}/>
            </React.Fragment>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));

