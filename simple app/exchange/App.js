const Cash = (props) => {
    const {cash, ratio, title} = props;
    const value = (cash / ratio * props.price).toFixed(2);
    return (
        <div>{title}{cash === 0 ? "" : value}</div>
    )
};

class Exchange extends React.Component {

    state = {
        amount: "",
        product: "gas"
    };

    static defaultProps = {
        currencies: [
            {
                id: 0,
                name: 'złoty',
                ratio: 1,
                title: 'Wartość w złotówkach:',
            },
            {
                id: 2,
                name: 'euro',
                ratio: 4.2,
                title: 'Wartość w euro:',
            },
            {
                id: 3,
                name: 'frank',
                ratio: 4.0,
                title: 'Wartość we frankach:',
            }
        ],
        price: {
            electricity: .51,
            gas: 4.76,
            oranges: 3.79
        }

    };

    handleChange = (e) => {
        this.setState({
            amount: e.target.value,
        })
    };

    handleSelect = (e) => {
        this.setState({
            product: e.target.value,
            amount: "",
        })
    };

    insertSuffix (select) {
        if(select === "electricity") return <em> kWh</em>;
        else if(select === "gas") return <em> L</em>;
        else if (select === "oranges") return <em> kg</em>;
        else return null
    };

    selectPrice(select) {
        return this.props.price[select];
    };

    render() {
        const {amount, product} = this.state;
        const price = this.selectPrice(product);

        const calculators = this.props.currencies.map(currency => (
            <Cash key={currency.id} ratio={currency.ratio} title={currency.title} cash={amount} price={price}/>
        ));

        return (
            <div className="app">
                <label>
                    Wybierz Produkt:
                    <br/>
                    <select value={product} onChange={this.handleSelect}>
                        <option value="electricity">prąd</option>
                        <option value="gas">gaz</option>
                        <option value="oranges">pomarańcze</option>
                    </select>
                </label>
                <label>
                    <br/>
                    <input
                        name="number"
                        type="number"
                        value={amount}
                        onChange={this.handleChange}
                    />
                    {this.insertSuffix(product)}
                </label>
                {calculators}
          </div>
        )
    }
}


